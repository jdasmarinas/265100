FROM python:3.9-slim-buster as base

## Just try to install some packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    locales \
    && rm -rf /var/lib/apt/lists/* \
    && locale-gen "en_US.UTF-8" \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LC_ALL="en_US.UTF-8" LANG="en_US.utf8"

RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils \
    ca-certificates \
    curl \
    g++ \
    gnupg2 \
    parallel \
    git-core \
    wget

RUN echo "this is a test message" > /tmp/test

FROM ubuntu:latest

RUN apt-get update && apt-get install -y --no-install-recommends coreutils
COPY README.md /mnt/README.md
RUN md5sum /mnt/README.md

## Just try to install some packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates curl

COPY --from=base /tmp/test /tmp/test

RUN echo "test message from the main stage" > /tmp/test2
